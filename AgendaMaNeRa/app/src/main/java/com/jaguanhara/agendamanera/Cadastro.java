package com.jaguanhara.agendamanera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaguanhara.agendamanera.modelo.Cliente;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Cadastro extends AppCompatActivity {

    EditText nome;
    EditText fone;
    EditText email;
    EditText niver;
    EditText cep;

    private List<Cliente> clienteList = new ArrayList<Cliente>();
    private boolean result = false;

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference dbRef ;

    //camera--------------------------------------
    private static final int CAMERA = 1;
    private ImageView imageView;
    private String caminhoDaImagem;
    private Uri uri;
    private File path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/AGENDA");
    private ImageView imgV;
    public File imagem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        nome = (EditText) findViewById(R.id.editNome);
        fone = (EditText) findViewById(R.id.editPhone);
        email = (EditText) findViewById(R.id.editEmail);
        niver = (EditText) findViewById(R.id.editData);
        cep = (EditText) findViewById(R.id.editCep);

        inicializaFireBase();
        carregaLista();

    }

    private void inicializaFireBase() {
        FirebaseApp.initializeApp(Cadastro.this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        dbRef = firebaseDatabase.getReference();
    }

    public void addCliente(View view) {

        carregaLista();
//        System.out.println("AQUI - tamanho lista"+clienteList.size());
        if(verificaCliente(email.getText().toString().trim())){
            Toast.makeText(this, "Cliente já cadastrado!!", Toast.LENGTH_SHORT).show();
        } else {

            Cliente cl = new Cliente();
            cl.setcId(UUID.randomUUID().toString());
            cl.setNome(nome.getText().toString());
            cl.setFone(fone.getText().toString());
            cl.setEmail(email.getText().toString());
            cl.setNiver(niver.getText().toString());
            cl.setCep(cep.getText().toString());

            dbRef.child("Cliente").child(cl.getcId()).setValue(cl);

            Toast.makeText(this, "Cliente adicionado!!!", Toast.LENGTH_LONG).show();

            finish();

        }
    }

    private void carregaLista() {
        dbRef.child("Cliente").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                clienteList.clear();
                for (DataSnapshot objSnap : dataSnapshot.getChildren()) {
                    Cliente cl = objSnap.getValue(Cliente.class);
                    clienteList.add(cl);
//                    System.out.println(cl);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private Boolean verificaCliente(final String em){
        result = false;
//        System.out.println("AQUI - tamanho lista"+clienteList.size());
        for(int i = 0;i < clienteList.size();i++){
//            System.out.println(clienteList.get(i).getEmail().toString().trim());
//            System.out.println("EMAIL: "+clienteList.get(i).getEmail());
            if(clienteList.get(i).getEmail().trim().equals(em.trim())){
                result = true;
//                System.out.println("AQUI Entrou");
                break;
            }
        }
        return result;
    }

    public void chamarMenu (View view){
        //fecha activity e retorna para anterior
        finish();
//        Intent ittmenu = new Intent( Cadastro.this, Menu.class);
//        startActivity(ittmenu);
    }

    // foto
    public void chamaFoto(View view) {
        if (!path.exists()) {
            path.mkdirs();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else {

            Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (intentCamera.resolveActivity(getPackageManager()) != null) {

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

                File diretorio = path;
                imagem = new File(diretorio.getPath() + "/" + timeStamp + ".jpg");

                Uri uri2 = FileProvider.getUriForFile(Cadastro.this, BuildConfig.APPLICATION_ID + ".provider", imagem);

                if (android.os.Build.VERSION.SDK_INT < 20) {
                    uri = Uri.fromFile(imagem);
                } else {
                    uri = uri2;
                }

                if (imagem != null) {
                    intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    intentCamera.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
//                    intentCamera.putExtra("android.intent.extras.CAMERA_FACING", CameraCharacteristics.LENS_FACING_FRONT);
                    intentCamera.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivityForResult(intentCamera, CAMERA);
                } else {

                }

            }
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        if(imagem != null){

            ImageView imv = (ImageView) findViewById(R.id.imFoto);

            Uri uriEnd = Uri.parse("file://" + imagem.getPath());
            imv.setImageURI(uriEnd);
        }
    }
}
