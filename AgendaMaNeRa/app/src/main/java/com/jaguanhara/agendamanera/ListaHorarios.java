package com.jaguanhara.agendamanera;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaguanhara.agendamanera.modelo.Horario;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListaHorarios extends AppCompatActivity {

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference dbRef ;

    private List<Horario> horarioList = new ArrayList<Horario>();
    private ArrayAdapter<Horario> arrayAdapterHorario;

    ListView listaLh;
    String dia;
    TextView tvDia;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_horarios);
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        Intent it = getIntent();

        Bundle extras = it.getExtras();
        if (extras != null) {
            dia = extras.getString("dia");
        }

        tvDia = (TextView) findViewById(R.id.tvDia);
        tvDia.setText(dia);

        listaLh = (ListView) findViewById(R.id.lvListaHorarios);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();

                Intent it = new Intent(ListaHorarios.this, AddHora.class);
                it.putExtra("dia",dia);
                startActivity(it);
            }
        });

        inicializaFireBase();
        carregaLista();

    }

    private void inicializaFireBase() {
        FirebaseApp.initializeApp(ListaHorarios.this);
        firebaseDatabase = FirebaseDatabase.getInstance();
//        firebaseDatabase.setPersistenceEnabled(true);
        dbRef = firebaseDatabase.getReference();
    }

    private void carregaLista() {
        dbRef.child("Horario").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                horarioList.clear();
                for (DataSnapshot objSnap:dataSnapshot.getChildren()){
                    Horario tf = objSnap.getValue(Horario.class);
                    if(tf.getData().trim().equals(dia.trim())){
                        horarioList.add(tf);
                    }
                }
                arrayAdapterHorario = new ArrayAdapter<Horario>(ListaHorarios.this, android.R.layout.simple_list_item_1, horarioList);
                listaLh.setAdapter(arrayAdapterHorario);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
