package com.jaguanhara.agendamanera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaguanhara.agendamanera.modelo.Cliente;
import com.jaguanhara.agendamanera.modelo.Horario;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AddHora extends AppCompatActivity {

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference dbRef ;

    public String dia;
    public TextView tvDt;

    public Spinner clienteSp;
    public ArrayAdapter adapterCli;
//    public ArrayList<String> clientes;
    public Cliente cliente;

    public List<Cliente> li = new ArrayList<Cliente>();

    public EditText etHorario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_hora);

        Intent it = getIntent();

        Bundle extras = it.getExtras();
        if (extras != null) {
            dia = extras.getString("dia");
        }

        tvDt = (TextView) findViewById(R.id.tvDt);
        tvDt.setText("Data: "+dia);

        etHorario = (EditText) findViewById(R.id.etHora);

        //Spinner

        clienteSp = (Spinner) findViewById(R.id.spCliente);

        inicializaFireBase();
        getListaClientes();

        clienteSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cliente = ((Cliente)clienteSp.getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void getListaClientes() {

        dbRef.child("Cliente").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                li.clear();
                for (DataSnapshot objSnap : dataSnapshot.getChildren()) {
                    Cliente cl = objSnap.getValue(Cliente.class);
                    li.add(cl);
                }

                adapterCli= new ArrayAdapter(AddHora.this, android.R.layout.simple_spinner_dropdown_item, li);
                clienteSp.setAdapter(adapterCli);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void inicializaFireBase() {
        FirebaseApp.initializeApp(AddHora.this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        dbRef = firebaseDatabase.getReference();
    }

    public void addHorario(View view) {

        Horario hr = new Horario();
        hr.sethId(UUID.randomUUID().toString());
        hr.setData(dia);
        hr.setHora(etHorario.getText().toString());
        hr.setCliente(cliente);

        dbRef.child("Horario").child(hr.gethId()).setValue(hr);

        Toast.makeText(this, "Horário adicionado!!!", Toast.LENGTH_LONG).show();

        finish();

    }

}
