package com.jaguanhara.agendamanera;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Calendario extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendario);

        final CalendarView calendarView = (CalendarView) findViewById(R.id.calendarView);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {

//                Toast.makeText(Calendario.this, "Data" + dayOfMonth + "/" + month + "/" + year, Toast.LENGTH_LONG).show();

                Intent it = new Intent(Calendario.this, ListaHorarios.class);
                it.putExtra("dia",dayOfMonth + "/" + (month+1) + "/" + year);
                startActivity(it);

//                Intent intent = new Intent(CalendarioActivity.this,
//                        ClasseCapturaDados.class);
//                intent.putExtra("dataLongMiliseconds",
//                        (Long) calendarView.getDate());
//                startActivity(intent);

            }
        });

    }
//    public void chamarMenu (View view){
//        Intent itt = new Intent( Calendario.this, Menu.class);
//        startActivity(itt);
//    }

    public void chamarCadastro (View view){
        Intent ittcad = new Intent( Calendario.this, Cadastro.class);
        startActivity(ittcad);
    }
}

