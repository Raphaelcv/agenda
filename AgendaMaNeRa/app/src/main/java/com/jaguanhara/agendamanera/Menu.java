package com.jaguanhara.agendamanera;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }
    public void chamarCadastro (View view){
        Intent ittcad = new Intent( Menu.this, Cadastro.class);
        startActivity(ittcad);
    }
    public void chamarCalendario (View view){
        finish();
//        Intent ittcal = new Intent( Menu.this, Calendario.class);
//        startActivity(ittcal);
    }
}
