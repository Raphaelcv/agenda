package com.jaguanhara.agendamanera.modelo;

public class Cliente {

    private String cId;
    private String email;
    private String fone;
    private String nome;
    private String niver;
    private String cep;

    public String getcId() {
        return cId;
    }

    public void setcId(String cId) {
        this.cId = cId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNiver() {
        return niver;
    }

    public void setNiver(String niver) {
        this.niver = niver;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    @Override
    public String toString() {
        return nome;
    }
}