package br.com.rphl.localizacao;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.overlay.Marker;


public class TelaMapa extends AppCompatActivity implements LocationListener {
    private org.osmdroid.views.MapView osm;
    private MapController mc;
    private LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        osm = (org.osmdroid.views.MapView) findViewById(R.id.mapview);
        osm.setTileSource(TileSourceFactory.MAPNIK);
        Configuration.getInstance().setUserAgentValue("github-glenn1wang-myapp");
        osm.setBuiltInZoomControls(true);
        osm.setMultiTouchControls(true);

        mc = (MapController) osm.getController();
        mc.setZoom(17);

//        GeoPoint center = new GeoPoint(-20.1698, -40.2487);
//        mc.animateTo(center);
//        addMarker(center);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);


    }

    public void addMarker(GeoPoint center){
        Marker marker = new Marker(osm);
        marker.setPosition(center);
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        marker.setIcon(getResources().getDrawable(R.drawable.ic_menu_mylocation));

        osm.getOverlays().clear();
        osm.getOverlays().add(marker);
        osm.invalidate();

    }

    @Override
    public void onLocationChanged(Location location) {
// mudança do posicionamento
        GeoPoint center = new GeoPoint(location.getLatitude(), location.getLongitude());

        mc.animateTo(center);
        addMarker(center);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
// perde atividade
    }

    @Override
    public void onProviderEnabled(String s) {
//GPS ativo
    }

    @Override
    public void onProviderDisabled(String s) {
//        alteração no estado do GPS
    }

//    para economizar bateria
    @Override
    public void onDestroy(){
        super.onDestroy();
        if(locationManager != null){
            locationManager.removeUpdates(this);
        }
    }
}
